﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebApplication_ltdTest.Models;
using WebApplication_ltdTest.Utilities;

namespace WebApplication_ltdTest
{
    public partial class list : Page
    {
        private static string listXmlPath = "~/App_Data/List.xml";

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        

        [WebMethod]
        public static Store GetData() //GetData function
        {
            XmlFileReader xmlreader = new XmlFileReader(listXmlPath);
            Store jStore = xmlreader.readXml();
            return jStore;
        }



    }
}