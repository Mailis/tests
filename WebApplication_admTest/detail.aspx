﻿<%@ Page Title="About" Language="C#" 
    MasterPageFile="~/Site.Master" 
    AutoEventWireup="true" 
    CodeBehind="detail.aspx.cs" 
    Inherits="WebApplication_ltdTest.detail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div class="thin_border caption_detail">
        
        <div class ="txt_margin detail_caption_txt">Product Details</div>
        
        <asp:FormView  ID="DetailPanel" runat="server" EnableViewState="false"
            ItemType="WebApplication_ltdTest.Models.DetailView" 
            SelectMethod ="displayDetails" 
            RenderOuterTable="false" >
            <ItemTemplate>  
                <div class ="thin_border detail_container" >
                    <div class="">
                        <%# DataBinder.Eval(Container.DataItem, "Title") %> 
                    </div>
                    <div class ="pr_title_image " >
                        <div class="float_left"><!-- >-->
                            <img class="img_dim img_detail" src='<%# DataBinder.Eval(Container.DataItem, "Image") %>' />
                        </div>
                        <div class ="thin_border pr_description_price float_right" >
                            <div class="txt_margin float_left text_ontop">
                                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                            </div>
                            <div class="thin_border float_right pr__price_box"><!-- >-->
                                Price: <%# DataBinder.Eval(Container.DataItem, "Price") %>
                            </div>
                            <div class="float_clear"></div>
                        </div>
                        <div class="float_clear"></div>
                    </div>
                    <div class="thin_border">
                        <div class="specifications_txt">Specifications</div>
                        <div class="spec_list_txt">
                            <asp:Repeater ID="sublist" runat="server" DataSource ="<%# Item.Specs %>"  ItemType="System.string">
                                  <ItemTemplate>
                                      <div>
                                          <%# Item %>
                                      </div>
                                  </ItemTemplate>
                            </asp:Repeater>  
                        </div>
                    </div>
                </div>
            
            </ItemTemplate>
        </asp:FormView>
        <br />

        
        <%--<asp:ImageButton ID="ImageButton1" runat="server" OnError="ImageButton1_Click" />--%>

        
    </div>
</asp:Content>
