﻿//replace missing image
function myFunction(see) {
    see.setAttribute("src", "Content/Images/missing.png");
}
//redirection to details
function showDetails(id) {
    window.location.href = "detail/" + id;
}


//GLOBAL CONSTANTS
//constants for filter types
function SortBy() {
    this.id = "Id";
    this.price = "Price";
    this.popularity = "Popular";
}
var sb = new SortBy();

//availability key
var availability = "Availability";

//constant for ordering  variable as string
var ascending_order = "ascending_order";
//constants for filter ordering  value and respective text
var filter_order_text = "filter_order_text";
var filter_order_value = "filter_order_value";




//PERSISTANCE variables
//persist initial products list through app lifecycle
var ProductsList = [];
//save availability
var ProductAvailability = {};


$(document).ready(function () {


    //INITIAL method calls
    fillProdFilterDropDownList();
    getProdAvailability();
    getProdList();




    //EVENTS
    $("#prod_filter_dropdown").change(function () {
        var filterValue = $(this).find(":selected").val();
        var filterText = $(this).find(":selected").text();
        filterText = (isBlank(filterText) ? sb.id : filterText);
        setFilterCookie(filterValue, filterText);
        reorder();
    });

    $("#sort_asc").click(function () {
        setOrderingCookie(true);
        reorder();
    });

    $("#sort_desc").click(function () {
        setOrderingCookie(false);
        reorder();
    });



    //AJAX functions

    //ajax POST
    function ajaxPost(_url, callbackFunction) {
        if (_url !== undefined) {
            $.ajax({
                type: "POST",
                url: _url,
                contentType: "application/json;charset=utf-8",
                data: {},
                dataType: "json",
                success: function (response) {
                    callbackFunction(response.d);
                },
                error: function () {
                    callbackFunction({});
                }
            });
        }
    }

    //product list from list.xml
    function ajaxCallBack(responseObject) {
        if (responseObject !== undefined) {
            var response = responseObject.Products;
            for (var i = 0; i < response.length; i++) {
                if (typeof (response[i]) === 'object') {
                    response[i]["Popular"] = response[i]["Sorting"]["Popular"];
                }
                //format money
                var money = response[i][sb.price];
                response[i][sb.price] = money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                //set availability
                var idKey = response[i][sb.id];
                if (idKey in ProductAvailability) {
                    response[i][availability] = ProductAvailability[idKey];
                }
                else {
                    response[i][availability] = "";
                }
            }
            ProductsList = response;

            reorder();
        }
    }


    //get list of products
    function getProdList() {
        ajaxPost("list.aspx/GetData", ajaxCallBack);
    }

    //product availability
    function availabilityCallBack(response) {
        ProductAvailability = response;
    }

    function getProdAvailability() {
        ajaxPost("detail.aspx/GetProductAvailability", availabilityCallBack);
    }


    //fill and display views

    function fillProdFilterDropDownList() {
        //template for dropdown list, 
        //at page list aspx
        var template = $("#prod_filter_template").html();
        //empty current list
        $("#prod_filter_dropdown").empty();
        //make values for options
        var i = 0;
        $.map(sb, function (value, index) {
            value = ((value === sb.id) ? " " : value);
            html = Mustache.render(template, { optval: i, filterString: value });
            i++;
            $("#prod_filter_dropdown").append(html);

            //get selected filter from cookie
            var selectedFilterValue = getFilterCookieValue();
            if (selectedFilterValue === undefined)
                var selectedFilterValue = 0;
            $("#prod_filter_dropdown").val(selectedFilterValue).change();
        });
    }


    //------------


    function reorder() {
        var filterCookieValue = getFilterCookieValue();
        var filterCookieText = getFilterCookieText();
        if (getFilterCookieValue() === undefined) {
            setFilterCookie(0, sb.id);
            filterCookieText = sb.id;
        }
        var orderCookie = getOrderingCookie();
        if (orderCookie === undefined) {
            setOrderingCookie(true);
            orderCookie = getOrderingCookie();
        }

        var isNUmeric = (filterCookieText == sb.id) ? false : true;
        //ProductsList
        var products = sortProperties(ProductsList, filterCookieText, isNUmeric, orderCookie);
        displayProductList(products);
    }



    function displayProductList(pList) {
        if (pList !== undefined) {
            $("#repeaterDemo").empty();
            if (pList.length > 0) {
                var template = $("#product_list").html();
                for (var i = 0; i < pList.length; i++) {
                    var listData = pList[i][1];
                    var html = Mustache.render(template, listData);
                    $("#repeaterDemo").append(
                        html
                    );
                }
            }
        }
    }



    //set-get cookies
    function setOrderingCookie(ordering) {
        document.cookie = ascending_order + " = " + ordering;
    }
    function getOrderingCookie() {
        return getCookie(ascending_order);
    }
    function setFilterCookie(filterValue, filterText) {
        document.cookie = filter_order_text + " = " + filterText;
        document.cookie = filter_order_value + " = " + filterValue;
    }
    function getFilterCookieText() {
        return getCookie(filter_order_text);
    }
    function getFilterCookieValue() {
        return getCookie(filter_order_value);
    }
    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        var typedValue = undefined;
        if (parts.length === 2) {
            var stringValue = parts.pop().split(";").shift();
            //get real boolean type
            if (stringValue === 'true') {
                typedValue = true;
            }
            else if (stringValue === 'false') {
                typedValue = false;
            }
            else {
                typedValue = stringValue;
            }
        }
        return typedValue;
    }

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }


});


//object sorting
/**
        * Sort object properties (only own properties will be sorted).
        * @param {object} obj object to sort properties
        * @param {string|int} sortedBy 1 - sort object properties by specific value.
        * @param {bool} isNumericSort true - sort object properties as numeric value, false - sort as string value.
        * @param {bool} reverse false - reverse sorting.
        * @returns {Object} object from (array of items in [[key,value],[key,value],...] format).
        */
function sortProperties(obj, sortedBy, isNumericSort, reverse) {
    sortedBy = sortedBy || 1; // by default first key
    isNumericSort = isNumericSort || false; // by default text sort
    reverse = reverse || false; // by default no reverse

    var reversed = (reverse) ? 1 : -1;

    var sortable = [];
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            sortable.push([key, obj[key]]);
        }
    }
    if (isNumericSort)
        sortable.sort(function (a, b) {
            return reversed * (a[1][sortedBy] - b[1][sortedBy]);
        });
    else
        sortable.sort(function (a, b) {
            var x = a[1][sortedBy].toLowerCase(),
                y = b[1][sortedBy].toLowerCase();
            return x < y ? reversed * -1 : x > y ? reversed : 0;
        });

    //sortable - array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    return sortable;
}




