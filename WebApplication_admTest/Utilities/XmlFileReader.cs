﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using WebApplication_ltdTest.Models;

namespace WebApplication_ltdTest.Utilities
{
    public class XmlFileReader
    {
        string xmlFilePath = null;
        public XmlFileReader(string _xmlFilePath)
        {
            this.xmlFilePath = HttpContext.Current.Server.MapPath(_xmlFilePath);
        }
        public Store readXml()
        {
            // Declares an object variable of the type to be deserialized.
            Store prodList = null;
            if (System.IO.File.Exists(this.xmlFilePath))
            {
                string filename = this.xmlFilePath;
                try
                {
                    // Creates an instance of the XmlSerializer class;
                    // specifies the type of object to be deserialized.
                    XmlSerializer serializer = new XmlSerializer(typeof(Store));
                    ////https://msdn.microsoft.com/en-us/library/58a18dwa(v=vs.110).aspx

                    // A FileStream is needed to read the XML document.
                    FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, bufferSize: 4096, useAsync: true);
                    // Uses the Deserialize method to restore the object's state 
                    // with data from the XML document. */
                    prodList = (Store)serializer.Deserialize(fs);
                    //release file resource
                    fs.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return prodList;
        }
    }
}