﻿<%@ Page Title="Home Page" Language="C#" 
    MasterPageFile="~/Site.Master" 
    AutoEventWireup="true" 
    CodeBehind="list.aspx.cs" 
    Inherits="WebApplication_ltdTest.list" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
    <script src="Scripts/ltd/productList.js" type="text/javascript"></script>
    <script>

</script>
    <script id="prod_filter_template" type="text/template">
        <option value = "{{{optval}}}">{{{filterString}}}</option>
    </script>
    <script id="product_list" type="text/template">
        <div class="thin_border container_width">
            <table class="table_auto">
                <tr>
                    <td class ="img_td">
                        <div class="float_left img_div">
                            <img class="img_dim" src='{{{Image}}}' onerror="myFunction(this)" />
                        </div>
                    </td>
                    <td>
                        <div class="float_left td_w">
                            <div class="txt_margin">
                                {{{Title}}}
                            </div>
                            <div class="txt_margin">
                                {{{Description}}}
                            </div>
                            <div class="txt_margin">
                                <input class="btn_details" type="button" value="More Details" onclick="showDetails('{{{Id }}}')" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="thin_border inner_box">
                            <div class="txt_margin">
                                Price: {{{Price}}} EUR
                            </div>
                            <div class="txt_margin">
                                Availability: {{{Availability}}}
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <br /><br />
    </script>
                       
    <div>
        <div class ="txt_margin">Product list</div>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate> 
                <div class ="thin_border  caption_margin ">
                    <table class="table_auto td_left">
                        <tr>
                        <td>
                            <span class="td_left_start">Sort by:</span>
                        </td>
                        <td>
                            <span class="td_left">
                                <select id = "prod_filter_dropdown"></select>
                            </span>
                        </td>
                        <td>
                            <div class="td_left">
                                <span class="imjusttext" ID="sort_asc" >ASC</span>
                                <span> / </span>
                                <span class="imjusttext" ID="sort_desc">DESC</span>
                            </div>
                        </td>
                        </tr>
                    </table>
                </div>
                <div class ="thin_border caption_margin_list">
                      <div id="repeaterDemo">
                      </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />

    </div>

</asp:Content>