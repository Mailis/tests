﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication_ltdTest.Models;
using WebApplication_ltdTest.Utilities;

namespace WebApplication_ltdTest
{
    public partial class detail : Page
    {
        public static Store dStore;
        private static string detailXmlPath = "~/App_Data/Detail.xml";
        private static string imgNotFound;
        protected void Page_Load(object sender, EventArgs e)
        {
            Uri uri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
            string requested = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
            imgNotFound = requested + "/Content/Images/missing.png";
        }
        

        private string getId(string uri)
        {
            return new Uri(uri).Segments.Last();
        }

        public DetailView displayDetails()
        {
            string uri = HttpContext.Current.Request.Url.AbsoluteUri;
            string productId = getId(uri);
            DetailView detailView = readDetail(productId);
            return detailView;
        }
        
     

        public void replaceMissingImage(object sender, EventArgs e)
        {

        }

        public static DetailView readDetail(string id)
        {
            if (dStore == null)
            {
                dStore = getDetails();
            }
            Store listStore = list.GetData();
            var innerJoinQuery =
                            from listp in listStore.Products
                            join detailp in dStore.Products on listp.Id equals detailp.Id
                            where listp.Id == id
                            select new DetailView {
                                Id = detailp.Id,
                                Title = detailp.Title,
                                Description = detailp.Description,
                                Image = ((File.Exists(detailp.Image) ? detailp.Image : imgNotFound)),
                                Price = listp.Price,
                                Specs = detailp.Specs
                            };
            DetailView dView = innerJoinQuery.ToList().First();
            return dView;
        }

        
        public static Store getDetails() //GetData function
        {
            XmlFileReader xmlreader = new XmlFileReader(detailXmlPath);
            dStore = xmlreader.readXml();
            return dStore;
        }

        [WebMethod]
        public static Dictionary<string, string> GetProductAvailability() //GetData function
        {
            if(dStore == null)
            {
                dStore = getDetails();
            }
            Dictionary<string, string> dir = new Dictionary<string, string>();
            try
            {
                foreach(Product p in dStore.Products)
                {
                    string pId = p.Id;
                    if (!dir.Keys.Contains(pId))
                    {
                        dir.Add(pId, p.Availability);
                    }
                }
                return dir;
            }
            catch
            {
                return dir;
            }
           
        }
        
    }
}