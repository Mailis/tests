﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication_ltdTest.Models
{
    [Serializable]
    [XmlRoot("Sorting")]
    public class Sorting
    {
        [XmlElement(DataType = "int")]
        public int Popular { get; set; }

    }
}