﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication_ltdTest.Models
{
    [Serializable]
    [XmlRoot("Product")]
    public class Product
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement(IsNullable = false)]
        public string Title { get; set; }

        [XmlElement(IsNullable = true)]
        public string Description { get; set; }

        [XmlElement(IsNullable = true)]
        public string Image { get; set; }

        [XmlElement(DataType="decimal")]
        public decimal Price { get; set; }

        [XmlElement(IsNullable = true)]
        public Sorting Sorting{ get; set; }

        [XmlArray("Specs")]
        [XmlArrayItem("Spec")]
        public List<String> Specs { get; set; }
        
        [XmlElement(IsNullable = true)]
        public string Availability { get; set; }


    }
}