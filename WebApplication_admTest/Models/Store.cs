﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication_ltdTest.Models
{
    [Serializable]
    [XmlRoot("Store")]
    public class Store
    {
        [XmlArray(IsNullable = true)]
        public List<Product> Products { get; set; }
    }
}